import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  authors: object[] =  [{id:1, author:'sahar'},{id:2, author:'Leo Tolstoy'}]
  newauthor:string;
  author;
  id;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;
  }

}
